FROM python:3.6-alpine3.7
#FROM python:3-onbuild

ENV PACKAGES="\
    libxml2-dev\
    libxslt-dev\
    python-dev\
    dumb-init \
    musl \
    libc6-compat \
    linux-headers \
    build-base \
    bash \
    tree\
    ca-certificates \
    freetype \
    libgfortran \
    libgcc \
    libstdc++ \
    openblas \
    tcl \
    tk \
    libssl1.0"

# update apk repo
RUN echo "http://dl-4.alpinelinux.org/alpine/v3.7/main" >> /etc/apk/repositories && \
    echo "http://dl-4.alpinelinux.org/alpine/v3.7/community" >> /etc/apk/repositories

RUN apk add --no-cache --virtual build-dependencies $PACKAGES

# install chromedriver
RUN apk update
RUN apk add chromium chromium-chromedriver	

#ENV PYTHONUNBUFFERED 1

# COPY requirements.txt /home/
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools

#RUN pip install --no-cache-dir $PYTHON_PACKAGES
RUN apk add py3-lxml
RUN apk update \
  && apk add --virtual .build-deps gcc python3-dev musl-dev libffi-dev \
  # TODO workaround start
  && apk del libressl-dev \
  && apk add openssl-dev \
  && pip install cryptography==2.2.2 \
  && apk del openssl-dev \
  && apk add libressl-dev

# Coping source in current directory into the image
#COPY . ./home

